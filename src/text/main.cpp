#include "../common/rnd.h"
#include "../common/grid.h"
#include "../common/solver.h"
#include "../common/maze.h"
#include <ctime>
#include <array>
#include <string>
#include <optional>
#include <string_view>
#include <vector>
#include <sstream>
#include <cassert>

#ifdef EMSCRIPTEN

#include <emscripten.h>
#include <emscripten/bind.h>

#endif

// GAME SPECIFIC CONSTANTS
#define GRID_X 10
#define GRID_Y 10

#define GRID_SIZE (GRID_X * GRID_Y)
#define QUEUE_SIZE (GRID_X * GRID_Y + 1)

std::string grid_render(const Grid &grid, bool renderPath);


enum Algorithm {
    BinarySearch,
    Sidewinder,
    AldousBolder,
    HuntAndKill,
    RecursiveBacktracker,
    Prims,
    WeightedPrims,
    Ellers,
    RecursiveDivisions,
};

std::string generate_maze(Algorithm algorithm, double seed, bool braid, bool renderPath);

#ifndef EMSCRIPTEN
int main(int argc, char **argv) {
    auto seed = (double)time(nullptr);
    bool braid = false;
    bool renderPath = true;

    const auto maze = generate_maze(Algorithm::RecursiveDivisions, seed, braid, renderPath);
    printf("%s", maze.c_str());

    return 0;
}
#else


EMSCRIPTEN_BINDINGS(maze) {
    emscripten::function("generateMaze", &generate_maze);

    emscripten::enum_<Algorithm>("MazeAlgorithm")
            .value("B__Search", Algorithm::BinarySearch)
            .value("Sidewinder", Algorithm::Sidewinder)
            .value("Aldous__Bolder", Algorithm::AldousBolder)
            .value("Hunt_and_Kill", Algorithm::HuntAndKill)
            .value("Recursive_Backgracker", Algorithm::RecursiveBacktracker)
            .value("Simplified_Prims", Algorithm::Prims)
            .value("Prims", Algorithm::WeightedPrims)
            .value("Ellers", Algorithm::Ellers)
            .value("Recursive_Divisions", Algorithm::RecursiveDivisions)
            ;
}

#endif

enum PieceIds {
    P_HORIZ,
    P_VERT,

    P_CORNER_UP_RIGHT,
    P_CORNER_UP_LEFT,
    P_CORNER_DOWN_RIGHT,
    P_CORNER_DOWN_LEFT,

    P_JOINT_LEFT_RIGHT_DOWN,
    P_JOINT_UP_DOWN_LEFT,
    P_JOINT_LEFT_RIGHT_UP,
    P_JOINT_UP_DOWN_RIGHT,
    P_JOINT_ALL,

    P_EMPTY,
    P_PATH
};

enum WALL_DIRS {
    H_LEFT = 0x1 << 0,
    H_RIGHT = 0x1 << 1,
    V_UP = 0x1 << 2,
    V_DOWN = 0x1 << 3,
};

static constexpr auto pieces = std::array{
    std::string_view{"───"},
    std::string_view{" │ "},

    std::string_view{" └─"},
    std::string_view{"─┘ "},
    std::string_view{" ┌─"},
    std::string_view{"─┐ "},

    std::string_view{"─┬─"},
    std::string_view{"─┤ "},
    std::string_view{"─┴─"},
    std::string_view{" ├─"},
    std::string_view{"─┼─"},
    std::string_view{"   "},
    std::string_view{" ● "},
};

static_assert(P_PATH == pieces.size() - 1);

static std::optional<Square> at_xy(const Grid& grid, int x, int y) {
    if (x < 0 || y < 0 || x >= grid.cols || y >= grid.rows) {
        return std::nullopt;
    }
    return *grid_xy(&grid, x, y);
}

static constexpr Square blankSquare = {.flags = 0, .distance = -1};

struct Cell {
    unsigned flags = 0u;
};

std::string grid_render(const Grid &grid, bool renderPath) {
    std::vector<std::vector<Cell>> walls;
    walls.resize(grid.rows * 2 + 1);

    for (auto& row : walls) {
        row.resize(grid.cols * 2 + 1);
    }
    for (int y = 0; y < grid.rows; ++y) {
        const int cellY = y * 2 + 1;
        for (int x = 0; x < grid.cols; ++x) {
            const int cellX = x * 2 + 1;
            const auto cur = at_xy(grid, x, y).value_or(blankSquare);

            const bool hasNorthWall = cur.flags & WALL_NORTH;
            const bool hasEastWall = cur.flags & WALL_EAST;
            const bool hasSouthWall = cur.flags & WALL_SOUTH;
            const bool hasWestWall = cur.flags & WALL_WEST;
            const bool isPath = cur.flags & SQUARE_ON_PATH;

            if (hasNorthWall) {
                walls[cellY - 1][cellX - 1].flags |= H_RIGHT;
                walls[cellY - 1][cellX + 0].flags |= H_RIGHT;
                walls[cellY - 1][cellX + 1].flags |= H_LEFT;
            }

            if (hasSouthWall) {
                walls[cellY + 1][cellX - 1].flags |= H_RIGHT;
                walls[cellY + 1][cellX + 0].flags |= H_RIGHT;
                walls[cellY + 1][cellX + 1].flags |= H_LEFT;
            }

            if (hasEastWall) {
                walls[cellY - 1][cellX + 1].flags |= V_DOWN;
                walls[cellY + 0][cellX + 1].flags |= V_DOWN;
                walls[cellY + 1][cellX + 1].flags |= V_UP;
            }

            if (hasWestWall) {
                walls[cellY - 1][cellX - 1].flags |= V_DOWN;
                walls[cellY + 0][cellX - 1].flags |= V_DOWN;
                walls[cellY + 1][cellX - 1].flags |= V_UP;
            }

            if (isPath) {
                const bool northIsPath = at_xy(grid, x, y - 1).value_or(blankSquare).flags & SQUARE_ON_PATH;
                const bool southIsPath = at_xy(grid, x, y + 1).value_or(blankSquare).flags & SQUARE_ON_PATH;
                const bool westIsPath = at_xy(grid, x - 1, y).value_or(blankSquare).flags & SQUARE_ON_PATH;
                const bool eastIsPath = at_xy(grid, x + 1, y).value_or(blankSquare).flags & SQUARE_ON_PATH;
                walls[cellY + 0][cellX + 0].flags |= SQUARE_ON_PATH;

                if (northIsPath) {
                    walls[cellY - 1][cellX + 0].flags |= SQUARE_ON_PATH;
                }
                if (southIsPath) {
                    walls[cellY + 1][cellX + 0].flags |= SQUARE_ON_PATH;
                }
                if (eastIsPath) {
                    walls[cellY + 0][cellX + 1].flags |= SQUARE_ON_PATH;
                }
                if (westIsPath) {
                    walls[cellY + 0][cellX - 1].flags |= SQUARE_ON_PATH;
                }
            }
        }
    }

    constexpr auto wallMask = WALL_EAST | WALL_NORTH | WALL_SOUTH | WALL_WEST;

    std::stringstream ss;

    for (const auto& row : walls) {
        for (const auto& cell : row) {
            if (cell.flags & wallMask) {
                if (cell.flags == wallMask) {
                    ss << pieces[P_JOINT_ALL];
                    continue;
                }

                const bool hasVertUp = cell.flags & V_UP;
                const bool hasVertDown = cell.flags & V_DOWN;
                const bool hasHorizLeft = cell.flags & H_LEFT;
                const bool hasHorizRight = cell.flags & H_RIGHT;

                if (hasHorizLeft && hasHorizRight && hasVertDown) {
                    ss << pieces[P_JOINT_LEFT_RIGHT_DOWN];
                }
                else if (hasHorizLeft && hasHorizRight && hasVertUp) {
                    ss << pieces[P_JOINT_LEFT_RIGHT_UP];
                }
                else if (hasVertUp && hasVertDown && hasHorizLeft) {
                    ss << pieces[P_JOINT_UP_DOWN_LEFT];
                }
                else if (hasVertUp && hasVertDown && hasHorizRight) {
                    ss << pieces[P_JOINT_UP_DOWN_RIGHT];
                }
                else if (hasVertDown && hasHorizLeft) {
                    ss << pieces[P_CORNER_DOWN_LEFT];
                }
                else if (hasVertDown && hasHorizRight) {
                    ss << pieces[P_CORNER_DOWN_RIGHT] ;
                }
                else if (hasVertUp && hasHorizLeft) {
                    ss << pieces[P_CORNER_UP_LEFT];
                }
                else if (hasVertUp && hasHorizRight) {
                    ss << pieces[P_CORNER_UP_RIGHT] ;
                }
                else if (hasVertUp || hasVertDown) {
                    ss << pieces[P_VERT];
                }
                else {
                    ss << pieces[P_HORIZ];
                }
            }
            else if (renderPath && (cell.flags & SQUARE_ON_PATH)) {
                ss << pieces[P_PATH];
            }
            else {
                ss << pieces[P_EMPTY];
            }
        }
        ss << "\n";
    }

    return ss.str();
}

std::string generate_maze(Algorithm algorithm, double seed, bool braid, bool renderPath) {
    Grid grid;
    Square squares[GRID_SIZE];
    grid_init_from(&grid, squares, GRID_SIZE, GRID_X, GRID_Y);

    mt_rnd rnd;
    mt_rnd_init(&rnd, static_cast<uint32_t>(seed));

    GridPoint start = {.x = 0, .y = GRID_Y - 1};
    GridPoint end = {.x = GRID_X - 1, .y = 0};

    GridAlgorithm algo = nullptr;
    switch (algorithm) {
        case BinarySearch:
            algo = maze_bsearch;
            break;
        case Sidewinder:
            algo = maze_sidewinder;
            break;
        case AldousBolder:
            algo = maze_aldous_broder;
            break;
        case HuntAndKill:
            algo = maze_hunt_and_kill;
            break;
        case RecursiveBacktracker:
            algo = maze_recursive_backtracker;
            break;
        case Prims:
            algo = maze_prims;
            break;
        case WeightedPrims:
            algo = maze_weighted_prims;
            break;
        case Ellers:
            algo = maze_ellers;
            break;
        case RecursiveDivisions:
            algo = maze_recursive_division;
            break;
    }
    assert(algo);
    maze_build(&grid, &rnd, algo);

    if (braid) {
        maze_remove_deadends(&grid, &rnd);
    }

    GridPoint queue[QUEUE_SIZE];
    solver_solve(&grid, start, end, queue, QUEUE_SIZE);

    const auto render = grid_render(grid, renderPath);
    return render;
}
