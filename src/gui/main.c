#include <time.h>
#include "SDL2/SDL.h"
#include <stdbool.h>
#include "../common/rnd.h"
#include "../common/grid.h"
#include "../common/solver.h"
#include "../common/maze.h"
#include <math.h>

#ifdef EMSCRIPTEN

#include <emscripten.h>
#include <emscripten/html5.h>

#endif

#define HEX_COL(H) { \
    .r = (uint8_t)((UINT32_C(H) >> 16) & 0xff), \
    .g = (uint8_t)((UINT32_C(H) >> 8) & 0xff), \
    .b = (uint8_t)((UINT32_C(H) >> 0) & 0xff), \
    .a = 0xff \
}


typedef struct {
    SDL_Surface *surface;
    SDL_Window *window;
} App;


// GAME SPECIFIC CONSTANTS
#define WIN_WIDTH 700
#define WIN_HEIGHT 700
#define SQUARE_SIZE 23
#define SQUARE_BORDER 1
#define BORDER_SQUARE (SQUARE_SIZE + SQUARE_BORDER * 2)
#define GRID_X (WIN_WIDTH / BORDER_SQUARE)
#define GRID_Y (WIN_HEIGHT / BORDER_SQUARE)
//#define GRID_X 5
//#define GRID_Y 5
const char *windowName = "Mazes";
const SDL_Color bgCol = HEX_COL(0xffffff);
const SDL_Color wallCol = HEX_COL(0x222222);
const SDL_Color characterColor = HEX_COL(0xff0000);
const SDL_Color goalColor = HEX_COL(0xf0b800);
const SDL_Color pathColor = HEX_COL(0xff8888);

#define GRID_SIZE (GRID_X * GRID_Y)
#define QUEUE_SIZE (GRID_X * GRID_Y + 1)

typedef struct State {
    Grid *grid;
    mt_rnd *rnd;
    GridPoint *start;
    GridPoint *end;
    GridPoint *queue;
    size_t queueSize;
    int *maxValue;
    bool renderWeights;
    bool renderPath;
} State;


bool init_app(App *app, const char **outErr);

void grid_render_weights(const Grid *grid, const App *app, int maxValue);

void grid_render_path(const Grid *grid, const App *app);

void grid_render_walls(const Grid *grid, const App *app);

void key_down_event(const SDL_Event *e, State *state);

static double hue_to_rgb(double p, double q, double t);

SDL_Color hsl_to_sdl(double hue, double saturation, double luminosity) {
    double r, g, b;

    hue /= 360.0;

    if (saturation == 0) {
        r = 1.0;
        g = 1.0;
        b = 1.0;
    } else {
        double q;
        if (luminosity < 0.5) {
            q = luminosity * (1 + saturation);
        } else {
            q = luminosity + saturation - luminosity * saturation;
        }
        double p = 2 * luminosity - q;
        r = hue_to_rgb(p, q, hue + 1.0 / 3.0) * 255;
        g = hue_to_rgb(p, q, hue) * 255;
        b = hue_to_rgb(p, q, hue - 1.0 / 3.0) * 255;
    }

    SDL_Color res = {
            .r = (uint8_t) (round(r)),
            .g = (uint8_t) (round(g)),
            .b = (uint8_t) (round(b)),
            .a = 0xff
    };
    return res;
}

static double hue_to_rgb(double p, double q, double t) {
    if (t < 0.0) {
        t += 1.0;
    }

    if (t > 1.0) {
        t -= 1.0;
    }

    if (t < 1.0 / 6.0) {
        return p + (q - p) * 6.0 * t;
    }

    if (t < 1.0 / 2.0) {
        return q;
    }

    if (t < 2.0 / 3.0) {
        return p + (q - p) * (2.0 / 3.0 - t) * 6.0;
    }

    return p;
}

void grid_render_character(const Grid* grid, const App* app);

GridAlgorithm lastAlgo = maze_weighted_prims;

int main(int argc, char **argv) {
    App app;
    const char *err;
    if (!init_app(&app, &err)) {
        printf("Unable to initialize! Reason: %s\n", err);
        return 1;
    }

    Grid grid;
    Square squares[GRID_SIZE];
    grid_init_from(&grid, squares, GRID_SIZE, GRID_X, GRID_Y);

    mt_rnd rnd;
    mt_rnd_init(&rnd, time(NULL));

    GridPoint start = {.x = 0, .y = GRID_Y - 1};
    GridPoint end = {
            .x = mt_rnd_uint(&rnd) % (grid.cols / 2) + grid.cols / 2,
            .y = mt_rnd_uint(&rnd) % (grid.rows / 2)
    };

    maze_build(&grid, &rnd, lastAlgo);

    GridPoint queue[QUEUE_SIZE];
    int maxValue = solver_solve(&grid, start, end, queue, QUEUE_SIZE);

    State gameState = {
            .grid = &grid,
            .rnd = &rnd,
            .start = &start,
            .end = &end,
            .queue = queue,
            .queueSize = QUEUE_SIZE,
            .maxValue = &maxValue,
            .renderWeights = false,
            .renderPath = false
    };

    while (true) {

        GridPoint oldStart = start;
        // process input
        SDL_Event e;
        while (SDL_PollEvent(&e)) {
            switch (e.type) {
                case SDL_QUIT:
                    goto close_program;
                case SDL_KEYDOWN:
                    key_down_event(&e, &gameState);
                    break;
            }
        }

        if (oldStart.x != start.x || oldStart.y != start.y) {
            solver_solve(&grid, start, end, queue, QUEUE_SIZE);
        }

        SDL_FillRect(app.surface, NULL, SDL_MapRGB(app.surface->format, bgCol.r, bgCol.g, bgCol.b));

        if (gameState.renderWeights) {
            grid_render_weights(&grid, &app, maxValue);
        }

        if (gameState.renderPath) {
            grid_render_path(&grid, &app);
        }
        grid_render_walls(&grid, &app);
        grid_render_character(&grid, &app);

        SDL_UpdateWindowSurface(app.window);
#ifdef EMSCRIPTEN
        emscripten_sleep(16);
#endif
    }

close_program:
    SDL_DestroyWindow(app.window);
    SDL_Quit();

    return 0;
}

void key_down_event(const SDL_Event *e, State *state) {
    bool builtMaze = false;

    if (e->key.keysym.sym == SDLK_1) {
        lastAlgo = maze_bsearch;
        builtMaze = true;
    }
    else if (e->key.keysym.sym == SDLK_2) {
        lastAlgo = maze_sidewinder;
        builtMaze = true;
    }
    else if (e->key.keysym.sym == SDLK_3) {
        lastAlgo = maze_aldous_broder;
        builtMaze = true;
    }
    else if (e->key.keysym.sym == SDLK_4) {
        lastAlgo = maze_hunt_and_kill;
        builtMaze = true;
    }
    else if (e->key.keysym.sym == SDLK_5) {
        lastAlgo = maze_recursive_backtracker;
        builtMaze = true;
    }
    else if (e->key.keysym.sym == SDLK_6) {
        lastAlgo = maze_prims;
        builtMaze = true;
    }
    else if (e->key.keysym.sym == SDLK_7) {
        lastAlgo = maze_weighted_prims;
        builtMaze = true;
    }
    else if (e->key.keysym.sym == SDLK_8) {
        lastAlgo = maze_ellers;
        builtMaze = true;
    }
    else if (e->key.keysym.sym == SDLK_9) {
        lastAlgo = maze_recursive_division;
        builtMaze = true;
    }
    else if (e->key.keysym.sym == SDLK_c) {
        state->renderWeights = !state->renderWeights;
        return;
    }
    else if (e->key.keysym.sym == SDLK_p) {
        state->renderPath = !state->renderPath;
        return;
    }

    if (builtMaze) {
        goto regenerate_maze;
    }

    const Square *curSquare = grid_xy(state->grid, state->start->x, state->start->y);
    const bool keyMoveUp = e->key.keysym.sym == SDLK_w || e->key.keysym.sym == SDLK_UP;
    const bool keyMoveLeft = e->key.keysym.sym == SDLK_a || e->key.keysym.sym == SDLK_LEFT;
    const bool keyMoveRight = e->key.keysym.sym == SDLK_d || e->key.keysym.sym == SDLK_RIGHT;
    const bool keyMoveDown = e->key.keysym.sym == SDLK_s || e->key.keysym.sym == SDLK_DOWN;

    const bool canMoveUp = state->start->y > 0 && !(curSquare->flags & WALL_NORTH);
    const bool canMoveLeft = state->start->x > 0 && !(curSquare->flags & WALL_WEST);
    const bool canMoveRight = state->start->x < state->grid->cols - 1 && !(curSquare->flags & WALL_EAST);
    const bool canMoveDown = state->start->y < state->grid->rows - 1 && !(curSquare->flags & WALL_SOUTH);

    if (keyMoveUp && canMoveUp) {
        state->start->y -= 1;
    } else if (keyMoveDown && canMoveDown) {
        state->start->y += 1;
    }
    if (keyMoveLeft && canMoveLeft) {
        state->start->x -= 1;
    }
    if (keyMoveRight && canMoveRight) {
        state->start->x += 1;
    }

    if (state->start->x == state->end->x && state->start->y == state->end->y) {
    regenerate_maze:
        {
            GridPoint s = {.x = 0, .y = GRID_Y - 1};
            *(state->start) = s;
            GridPoint newEnd = {
                    .x = mt_rnd_uint(state->rnd) % (state->grid->cols / 2) + state->grid->cols / 2,
                    .y = mt_rnd_uint(state->rnd) % (state->grid->rows / 2)
            };
            *(state->end) = newEnd;
            maze_build(state->grid, state->rnd, lastAlgo);
            *state->maxValue = solver_solve(state->grid, *state->start, *state->end, state->queue, state->queueSize);
            return;
        }
    }
    else if (e->key.keysym.sym == SDLK_b) {
        maze_remove_deadends(state->grid, state->rnd);
        *state->maxValue = solver_solve(state->grid, *state->start, *state->end, state->queue, state->queueSize);
    }
}

void grid_render_path(const Grid *grid, const App *app) {
    for (int y = 0; y < grid->rows; ++y) {
        for (int x = 0; x < grid->cols; ++x) {
            Square *square = grid_xy(grid, x, y);
            unsigned flags = square->flags;
            SDL_Color color = HEX_COL(0xffffff);

            if (flags & SQUARE_ON_PATH) {
                SDL_Color c = pathColor;
                color = c;
            } else {
                continue;
            }

            SDL_Rect rect = {
                    .x = x * BORDER_SQUARE + 5,
                    .y = y * BORDER_SQUARE + 5,
                    .w = BORDER_SQUARE - 10,
                    .h = BORDER_SQUARE - 10,
            };

            SDL_FillRect(app->surface, &rect, SDL_MapRGB(app->surface->format, color.r, color.g, color.b));
        }
    }
}

void grid_render_character(const Grid* grid, const App* app) {
    for (int y = 0; y < grid->rows; ++y) {
        for (int x = 0; x < grid->cols; ++x) {
            Square *square = grid_xy(grid, x, y);

            unsigned flags = square->flags;
            SDL_Color color;

            if (flags & SQUARE_START) {
                SDL_Color c = characterColor;
                color = c;
            }
            else if (flags & SQUARE_END) {
                SDL_Color c = goalColor;
                color = c;
            }
            else {
                continue;
            }

            SDL_Rect rect = {
                    .x = x * BORDER_SQUARE + 3,
                    .y = y * BORDER_SQUARE + 3,
                    .w = BORDER_SQUARE - 6,
                    .h = BORDER_SQUARE - 6,
            };

            SDL_FillRect(app->surface, &rect, SDL_MapRGB(app->surface->format, color.r, color.g, color.b));
        }
    }
}

void grid_render_weights(const Grid *grid, const App *app, int maxValue) {
    double maxV = maxValue;
    for (int y = 0; y < grid->rows; ++y) {
        for (int x = 0; x < grid->cols; ++x) {
            Square *square = grid_xy(grid, x, y);

            unsigned flags = square->flags;
            int curValue = square->distance;

            double curV = curValue;
            double perc = curV / maxV;

            const double minHue = 140.0;
            const double maxHue = 290.0;
            const double h = maxHue - minHue;
            double hue = minHue + (h * perc);
            if (hue < minHue) {
                hue = minHue;
            }
            if (hue > maxHue) {
                hue = maxHue;
            }

            double saturation = 1.0;

            const double maxLum = 0.6;
            const double minLum = 0.1;
            const double lum = maxLum - minLum;
            double luminosity = minLum + (lum - lum * perc);
            if (luminosity < minLum) {
                luminosity = minLum;
            }
            if (luminosity > maxLum) {
                luminosity = maxLum;
            }

            SDL_Color color = hsl_to_sdl(hue, saturation, luminosity);

            SDL_Rect rect = {
                    .x = x * BORDER_SQUARE,
                    .y = y * BORDER_SQUARE,
                    .w = BORDER_SQUARE,
                    .h = BORDER_SQUARE,
            };

            SDL_FillRect(app->surface, &rect, SDL_MapRGB(app->surface->format, color.r, color.g, color.b));
        }
    }
}

void grid_render_walls(const Grid *grid, const App *app) {
    uint32_t color = SDL_MapRGB(app->surface->format, wallCol.r, wallCol.g, wallCol.b);
    SDL_Rect border[4] = {
            {.x = 0, .y = 0, .h = 1, .w = WIN_HEIGHT},
            {.x = 0, .y = 0, .h = WIN_HEIGHT, .w = 1},
            {.x = 0, .y = WIN_HEIGHT, .h = 1, .w = WIN_HEIGHT},
            {.x = WIN_WIDTH, .y = 0, .h = WIN_HEIGHT, .w = 1},
    };

    SDL_FillRects(app->surface, border, 4, color);

    for (int y = 0; y < grid->rows; ++y) {
        for (int x = 0; x < grid->cols; ++x) {
            const Square *square = grid_xy(grid, x, y);
            const unsigned walls = square->flags;

            if (walls & WALL_NORTH) {
                SDL_Rect r = {.x = x * BORDER_SQUARE, .y = y * BORDER_SQUARE, .w = BORDER_SQUARE, .h = 1};
                SDL_FillRect(app->surface, &r, color);
            }

            if (walls & WALL_SOUTH) {
                SDL_Rect r = {.x = x * BORDER_SQUARE, .y = (y + 1) * BORDER_SQUARE, .w = BORDER_SQUARE, .h = 1};
                SDL_FillRect(app->surface, &r, color);
            }

            if (walls & WALL_EAST) {
                SDL_Rect r = {.x = (x + 1) * BORDER_SQUARE, .y = y * BORDER_SQUARE, .w = 1, .h = BORDER_SQUARE};
                SDL_FillRect(app->surface, &r, color);
            }

            if (walls & WALL_WEST) {
                SDL_Rect r = {.x = x * BORDER_SQUARE, .y = y * BORDER_SQUARE, .w = 1, .h = BORDER_SQUARE};
                SDL_FillRect(app->surface, &r, color);
            }
        }
    }
}

bool init_app(App *app, const char **outErr) {
    if (!app) {
        if (outErr) {
            *outErr = "Cannot initialize null!";
        }
        return false;
    }

    app->surface = NULL;
    app->window = NULL;

#ifndef __EMSCRIPTEN__
    int renderFlags = SDL_RENDERER_ACCELERATED;
#else
    int renderFlags = SDL_RENDERER_SOFTWARE;
#endif
    int winFlags = 0;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        goto init_sdl_err;
    }

    app->window = SDL_CreateWindow(windowName, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIN_WIDTH, WIN_HEIGHT,
                                   winFlags);
    if (!app->window) {
        goto init_sdl_err;
    }

    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

    app->surface = SDL_GetWindowSurface(app->window);
    if (!app->surface) {
        goto init_sdl_err;
    }

    if (outErr) {
        *outErr = NULL;
    }
    return true;

    init_sdl_err: // LABEL
    if (outErr) {
        *outErr = SDL_GetError();
    }
    return false;
}
