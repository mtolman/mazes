#ifndef SDL_DEMO_MAZE_H
#define SDL_DEMO_MAZE_H

#include "grid.h"
#include "rnd.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void(*GridAlgorithm)(Grid *grid, mt_rnd *rnd);

void maze_build(Grid *grid, mt_rnd *rnd, GridAlgorithm algorithm);

void maze_bsearch(Grid *grid, mt_rnd *rnd);

void maze_sidewinder(Grid *grid, mt_rnd *rnd);

void maze_aldous_broder(Grid* grid, mt_rnd *rnd);

void maze_hunt_and_kill(Grid* grid, mt_rnd *rnd);

void maze_recursive_backtracker(Grid* grid, mt_rnd *rnd);

void maze_prims(Grid* grid, mt_rnd* rnd);

void maze_weighted_prims(Grid* grid, mt_rnd* rnd);

void maze_remove_deadends(Grid* grid, mt_rnd* rnd);

void maze_ellers(Grid* grid, mt_rnd* rnd);

void maze_recursive_division(Grid* grid, mt_rnd* rnd);

#ifdef __cplusplus
};
#endif

#endif //SDL_DEMO_MAZE_H
