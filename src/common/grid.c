#include "grid.h"
#include <assert.h>

void grid_init_from(Grid *outGrid, Square * squares, size_t numSquares, size_t rows, size_t cols) {
    assert(squares);
    assert(rows * cols <= numSquares);
    assert(rows >= 1);
    assert(cols >= 1);
    outGrid->rows = rows;
    outGrid->cols = cols;
    outGrid->num_squares = rows * cols;
    outGrid->squares = squares;
    grid_clear(outGrid);
}

void grid_init(Grid *outGrid, size_t maxX, size_t maxY, Allocator* alloc) {
    assert(maxY >= 1);
    assert(maxX >= 1);
    outGrid->rows = maxY;
    outGrid->cols = maxX;
    outGrid->num_squares = maxY * maxX;
    outGrid->squares = alloc_mem(sizeof(Square) * outGrid->num_squares, alloc);
    grid_clear(outGrid);
}

void grid_fill(Grid *grid, Square with) {
    assert(grid);
    for (unsigned i = 0; i < grid->num_squares; ++i) {
        grid->squares[i] = with;
    }
}

void grid_clear(Grid *grid) {
    assert(grid);
    Square walls = {.flags = WALL_NORTH | WALL_EAST | WALL_SOUTH | WALL_WEST, .distance = -1};
    grid_fill(grid, walls);
}

void grid_clear_solve(Grid *grid) {
    assert(grid);
    for (size_t i = 0; i < grid->num_squares; ++i) {
        grid->squares[i].distance = -1;
        grid->squares[i].flags &= 0xf;
    }
}

void grid_free(Grid *grid, Allocator* alloc) {
    assert(grid);
    free_mem(grid->squares, alloc);
    grid->squares = NULL;
    grid->rows = 0;
    grid->cols = 0;
    grid->num_squares = 0;
}

bool grid_can_move_to(const Grid *grid, GridPoint a, GridPoint b) {
    if (a.x + 1 == b.x && a.y == b.y) {
        return !(grid_xy(grid, a.x, a.y)->flags & WALL_EAST);
    } else if (a.x - 1 == b.x && a.y == b.y) {
        return !(grid_xy(grid, a.x, a.y)->flags & WALL_WEST);
    } else if (a.y - 1 == b.y && a.x == b.x) {
        return !(grid_xy(grid, a.x, a.y)->flags & WALL_NORTH);
    } else if (a.y + 1 == b.y && a.x == b.x) {
        return !(grid_xy(grid, a.x, a.y)->flags & WALL_SOUTH);
    }

    return false;
}

Square *grid_xy(const Grid *grid, size_t x, size_t y) {
    assert(y < grid->rows);
    assert(x < grid->cols);
    return grid_index(grid, y * grid->cols + x);
}

Square *grid_index(const Grid *grid, size_t index) {
    assert(index < grid->num_squares);
    return &grid->squares[index];
}

Square *grid_at(const Grid *grid, GridPoint p) {
    assert(grid);
    return grid_xy(grid, p.x, p.y);
}

void grid_carve(Grid *grid, size_t x, size_t y, SquareWalls wall) {
    assert(grid);
    size_t nx = x, ny = y;
    SquareWalls wallPair = wall;

    switch (wall) {
        case WALL_NORTH:
            ny--;
            wallPair = WALL_SOUTH;
            break;
        case WALL_SOUTH:
            ny++;
            wallPair = WALL_NORTH;
            break;
        case WALL_WEST:
            nx--;
            wallPair = WALL_EAST;
            break;
        case WALL_EAST:
            nx++;
            wallPair = WALL_WEST;
            break;
    }
    assert(nx != x || ny != y);
    if (nx >= grid->cols || ny >= grid->cols) {
        return;
    }

    Square *ptr = grid_xy(grid, x, y);
    ptr->flags = ptr->flags & (~wall);

    ptr = grid_xy(grid, nx, ny);
    ptr->flags = ptr->flags & (~wallPair);
}

size_t grid_get_neighbors(const Grid *grid, const GridPoint *point, GridPoint *outPoints, size_t numOutPoints) {
    assert(grid);
    assert(point);
    assert(outPoints);
    assert(numOutPoints);

    size_t outIndex = 0;
#define ADD_POINT(X, Y) GridPoint res = {.x = X, .y = Y}; \
    assert(res.x < grid->cols); \
    assert(res.y < grid->rows); \
    outPoints[outIndex++] = res;

    if (point->x > 0 && outIndex < numOutPoints) {
        ADD_POINT(point->x - 1, point->y)
    }

    if (point->x < grid->cols - 1 && outIndex < numOutPoints) {
        ADD_POINT(point->x + 1, point->y)
    }

    if (point->y > 0 && outIndex < numOutPoints) {
        ADD_POINT(point->x, point->y - 1)
    }

    if (point->y < grid->rows - 1 && outIndex < numOutPoints) {
        ADD_POINT(point->x, point->y + 1)
    }

#undef ADD_POINT
    return outIndex;
}

bool grid_carve_to(Grid *grid, GridPoint a, GridPoint b) {
    assert(grid);
    if (a.x + 1 == b.x && a.y == b.y) {
        grid_carve(grid, a.x, a.y, WALL_EAST);
        return true;
    } else if (a.x - 1 == b.x && a.y == b.y) {
        grid_carve(grid, a.x, a.y, WALL_WEST);
        return true;
    } else if (a.y - 1 == b.y && a.x == b.x) {
        grid_carve(grid, a.x, a.y, WALL_NORTH);
        return true;
    } else if (a.y + 1 == b.y && a.x == b.x) {
        grid_carve(grid, a.x, a.y, WALL_SOUTH);
        return true;
    }

    return false;
}

size_t grid_count_deadends(const Grid* grid) {
    assert(grid);
    size_t count = 0;
    GridPoint neighbors[4];
    size_t numNeighbors;
    for (size_t y = 0; y < grid->rows; ++y) {
        for (size_t x = 0; x < grid->cols; ++x) {
            GridPoint gp = {.x = x, .y = y};
            numNeighbors = grid_get_neighbors(grid, &gp, neighbors, 4);

            size_t conns = 0;
            for (size_t n = 0; n < numNeighbors; ++n) {
                if (grid_can_move_to(grid, gp, neighbors[n])) {
                    ++conns;
                }
            }

            if (conns <= 1) {
                ++count;
            }
        }
    }
    return count;
}

void grid_separate(Grid *grid, size_t x, size_t y, SquareWalls wall) {
    assert(grid);
    size_t nx = x, ny = y;
    SquareWalls wallPair = wall;

    switch (wall) {
        case WALL_NORTH:
            ny--;
            wallPair = WALL_SOUTH;
            break;
        case WALL_SOUTH:
            ny++;
            wallPair = WALL_NORTH;
            break;
        case WALL_WEST:
            nx--;
            wallPair = WALL_EAST;
            break;
        case WALL_EAST:
            nx++;
            wallPair = WALL_WEST;
            break;
    }
    assert(nx != x || ny != y);
    Square *ptr = grid_xy(grid, x, y);
    ptr->flags |= wall;

    if (nx >= grid->cols || ny >= grid->cols) {
        return;
    }

    ptr = grid_xy(grid, nx, ny);
    ptr->flags |= wallPair;
}

void grid_ensure_border_walls(Grid *grid) {
    for (size_t x = 0; x < grid->cols; ++x) {
        grid_separate(grid, x, 0, WALL_NORTH);
    }
    for (size_t x = 0; x < grid->cols; ++x) {
        grid_separate(grid, x, grid->rows - 1, WALL_SOUTH);
    }

    for (size_t y = 0; y < grid->rows; ++y) {
        grid_separate(grid, 0, y, WALL_WEST);
    }
    for (size_t y = 0; y < grid->rows; ++y) {
        grid_separate(grid, grid->cols - 1, y, WALL_EAST);
    }
}
