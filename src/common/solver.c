#include "solver.h"
#include <assert.h>

int solver_solve(Grid *grid, GridPoint start, GridPoint end, GridPoint* queue, size_t queueSize) {
    assert(queueSize >= grid->rows * grid->cols);
    grid_clear_solve(grid);
    grid_xy(grid, start.x, start.y)->flags |= SQUARE_START;
    grid_xy(grid, start.x, start.y)->flags |= SQUARE_ON_PATH;

    grid_xy(grid, end.x, end.y)->flags |= SQUARE_END;
    grid_xy(grid, end.x, end.y)->flags |= SQUARE_ON_PATH;

    GridPoint *queueHead = queue;
    GridPoint *queueTail = queue;

    *queueTail = start;
    ++queueTail;

    GridPoint neighbors[4];
    size_t numNeighbors;

    grid_xy(grid, start.x, start.y)->distance = 0;
    int maxValue = 0;

    while (queueHead != queueTail) {
        assert(queueHead < queue + queueSize);
        assert(queueHead < queueTail);
        assert(queueHead >= queue);
        GridPoint cur = *queueHead;
        ++queueHead;
        const int curDistance = grid_xy(grid, cur.x, cur.y)->distance;

        numNeighbors = grid_get_neighbors(grid, &cur, neighbors, 4);
        for (size_t n = 0; n < numNeighbors; ++n) {
            Square *neighbor = grid_xy(grid, neighbors[n].x, neighbors[n].y);
            if (!neighbor || neighbor->distance >= 0 || !grid_can_move_to(grid, cur, neighbors[n])) {
                continue;
            }

            neighbor->distance = curDistance + 1;

            assert(queueTail < queue + queueSize);
            assert(queueTail >= queue);
            assert(queueTail >= queueHead);

            *queueTail = neighbors[n];
            ++queueTail;

            assert(queueTail <= queue + queueSize);
            if (curDistance + 1 > maxValue) {
                maxValue = curDistance + 1;
            }
        }
    }

    // Mark the path
    GridPoint cur = end;
    while (cur.x != start.x || cur.y != start.y) {
        numNeighbors = grid_get_neighbors(grid, &cur, neighbors, 4);
        GridPoint next = cur;
        int minValue = INT32_MAX;
        for (size_t n = 0; n < numNeighbors; ++n) {
            int dist = grid_xy(grid, neighbors[n].x, neighbors[n].y)->distance;
            if (dist < minValue && grid_can_move_to(grid, cur, neighbors[n])) {
                minValue = dist;
                next = neighbors[n];
            }
        }
        if (next.x == cur.x && next.y == cur.y) {
            break;
        }
        grid_xy(grid, next.x, next.y)->flags |= SQUARE_ON_PATH;
        cur = next;
    }

    return maxValue;
}
