#include <assert.h>
#include "maze.h"
#include "solver.h"

void maze_build(Grid *grid, mt_rnd *rnd, GridAlgorithm algorithm) {
    assert(grid);
    assert(rnd);

    grid_clear(grid);
    algorithm(grid, rnd);
}

void maze_sidewinder(Grid *grid, mt_rnd *rnd) {
    assert(grid);
    assert(rnd);

    for (unsigned yi = grid->rows; yi > 1; --yi) {
        unsigned y = yi - 1;
        unsigned runStart = 0;

        for (unsigned x = 0; x < grid->cols; ++x) {
            if (x + 1 < grid->cols && mt_rnd_float(rnd) < 0.5f) {
                grid_carve(grid, x, y, WALL_EAST);
                continue;
            }

            grid_carve(grid, mt_rnd_uint(rnd) % (x - runStart + 1) + runStart, y, WALL_NORTH);
            runStart = x + 1;
        }

        if (runStart < grid->cols) {
            grid_carve(grid, mt_rnd_uint(rnd) % ((grid->cols - 1) - runStart + 1) + runStart, y, WALL_NORTH);
        }
    }

    for (unsigned x = 0; x < grid->cols - 1; ++x) {
        grid_carve(grid, x, 0, WALL_EAST);
    }
}

void maze_bsearch(Grid *grid, mt_rnd *rnd) {
    assert(grid);
    assert(rnd);

    for (unsigned y = 1; y < grid->rows; ++y) {
        for (unsigned x = 0; x < grid->cols - 1; ++x) {
            SquareWalls dig = (mt_rnd_float(rnd) < 0.5f) ? WALL_NORTH : WALL_EAST;
            grid_carve(grid, x, y, dig);
        }
    }

    for (unsigned x = 0; x < grid->cols; ++x) {
        grid_carve(grid, x, 0, WALL_EAST);
    }

    for (unsigned y = 0; y < grid->rows; ++y) {
        grid_carve(grid, grid->cols - 1, y, WALL_NORTH);
    }
}

// Temporary flag for use by the algorithms
// Just needs to be high enough to not interfere with walls
static const unsigned SQUARE_VISITED = 0x800;

void maze_aldous_broder(Grid* grid, mt_rnd *rnd) {
    assert(grid);
    assert(rnd);

    grid_clear(grid);
    size_t numVisited = 1;

    GridPoint curPoint = {
            .x = mt_rnd_uint(rnd) % grid->cols,
            .y = mt_rnd_uint(rnd) % grid->rows,
    };

    assert(curPoint.x < grid->cols);
    assert(curPoint.y < grid->rows);
    GridPoint neighbors[4];
    size_t numNeighbors;

    while (numVisited < grid->num_squares) {
        grid_xy(grid, curPoint.x, curPoint.y)->flags |= SQUARE_VISITED;
        numNeighbors = grid_get_neighbors(grid, &curPoint, neighbors, 4);
        GridPoint nextNeighbor = neighbors[mt_rnd_uint(rnd) % numNeighbors];
        if (!(grid_xy(grid, nextNeighbor.x, nextNeighbor.y)->flags & SQUARE_VISITED)) {
            bool res = grid_carve_to(grid, curPoint, nextNeighbor);
            assert(res);
            ++numVisited;
        }
        curPoint = nextNeighbor;
    }

    // Clear our temporary data to avoid messing up solves
    grid_clear_solve(grid);
}

void maze_hunt_and_kill(Grid* grid, mt_rnd *rnd) {
    assert(grid);
    assert(rnd);

    grid_clear(grid);
    size_t numVisited = 0;

    GridPoint lastPoint = {
            .x = mt_rnd_uint(rnd) % grid->cols,
            .y = mt_rnd_uint(rnd) % grid->rows,
    };

    assert(lastPoint.x < grid->cols);
    assert(lastPoint.y < grid->rows);
    GridPoint neighbors[4];
    GridPoint notVisitedNeighbors[4];
    size_t numNeighbors;
    size_t numNotVisitedNeighbors;

    grid_at(grid, lastPoint)->flags |= SQUARE_VISITED;

    numNeighbors = grid_get_neighbors(grid, &lastPoint, neighbors, 4);
    GridPoint curPoint = neighbors[mt_rnd_uint(rnd) % numNeighbors];

    while (numVisited < grid->num_squares) {
        grid_at(grid, curPoint)->flags |= SQUARE_VISITED;
        ++numVisited;
        bool carved = grid_carve_to(grid, lastPoint, curPoint);
        assert(carved);
        numNeighbors = grid_get_neighbors(grid, &curPoint, neighbors, 4);

        numNotVisitedNeighbors = 0;

        for (size_t n = 0; n < numNeighbors; ++n) {
            if (!(grid_at(grid, neighbors[n])->flags & SQUARE_VISITED)) {
                notVisitedNeighbors[numNotVisitedNeighbors++] = neighbors[n];
            }
        }

        if (numNotVisitedNeighbors > 0) {
            lastPoint = curPoint;
            curPoint = notVisitedNeighbors[mt_rnd_uint(rnd) % numNotVisitedNeighbors];
            continue;
        }

        for (size_t y = 0; y < grid->rows; ++y) {
            for (size_t x = 0; x < grid->cols; ++x) {
                GridPoint cp = {.x = x, .y = y};

                if (!(grid_at(grid, cp)->flags & SQUARE_VISITED)) {
                    numNeighbors = grid_get_neighbors(grid, &cp, neighbors, 4);
                    for (size_t n = 0; n < numNeighbors; ++n) {
                        if (grid_at(grid, neighbors[n])->flags & SQUARE_VISITED) {
                            lastPoint = neighbors[n];
                            curPoint = cp;
                            goto hunt_kill_search_end;
                        }
                    }
                }
            }
        }
        break;
hunt_kill_search_end:
        continue;
    }

    // Clear our temporary data to avoid messing up solves
    grid_clear_solve(grid);
}

void maze_recursive_backtracker(Grid* grid, mt_rnd *rnd) {
    assert(grid);
    assert(rnd);

    grid_clear(grid);

    Allocator a;
    malloc_allocator(&a);

    GridPoint *stack = alloc_mem(sizeof (GridPoint) * (grid->num_squares + 1), &a);
    GridPoint *stackHead = stack;
    size_t numVisited = 0;

    GridPoint start = {
            .x = mt_rnd_uint(rnd) % grid->cols,
            .y = mt_rnd_uint(rnd) % grid->rows,
    };

    grid_at(grid, start)->flags |= SQUARE_VISITED;
    *stackHead = start;
    ++stackHead;
    ++numVisited;

    GridPoint neighbors[4];
    GridPoint notVisitedNeighbors[4];
    size_t numNeighbors;
    size_t numNotVisitedNeighbors;

    while (stackHead != stack && numVisited < grid->num_squares) {
        GridPoint root = *(stackHead - 1);
        numNeighbors = grid_get_neighbors(grid, &root, neighbors, 4);

        numNotVisitedNeighbors = 0;

        for (size_t n = 0; n < numNeighbors; ++n) {
            if (!(grid_at(grid, neighbors[n])->flags & SQUARE_VISITED)) {
                notVisitedNeighbors[numNotVisitedNeighbors++] = neighbors[n];
            }
        }

        if (numNotVisitedNeighbors > 0) {
            GridPoint next = notVisitedNeighbors[mt_rnd_uint(rnd) % numNotVisitedNeighbors];
            grid_carve_to(grid, root, next);
            grid_at(grid, next)->flags |= SQUARE_VISITED;
            *stackHead = next;
            ++stackHead;
            ++numVisited;
        }
        else {
            --stackHead;
        }
    }

    free_mem(stack, &a);
    release_allocator(&a);

    // Clear our temporary data to avoid messing up solves
    grid_clear_solve(grid);
}

void maze_remove_deadends(Grid* grid, mt_rnd* rnd) {
    assert(grid);
    assert(rnd);

    GridPoint neighbors[4];
    size_t numNeighbors;

    GridPoint disconnectedNeighbors[4];
    size_t numDisconnected;

    for (size_t y = 0; y < grid->rows; ++y) {
        for (size_t x = 0; x < grid->cols; ++x) {
            GridPoint gp = {.x = x, .y = y};
            numNeighbors = grid_get_neighbors(grid, &gp, neighbors, 4);

            size_t conns = 0;
            for (size_t n = 0; n < numNeighbors; ++n) {
                if (grid_can_move_to(grid, gp, neighbors[n])) {
                    ++conns;
                }
            }

            if (conns <= 1) {
                numDisconnected = 0;
                for (size_t n = 0; n < numNeighbors; ++n) {
                    if (!grid_can_move_to(grid, gp, neighbors[n])) {
                        disconnectedNeighbors[numDisconnected++] = neighbors[n];
                    }
                }
                assert(numDisconnected > 0);
                grid_carve_to(grid, gp, disconnectedNeighbors[mt_rnd_uint(rnd) % numDisconnected]);
            }
        }
    }
}

void maze_prims(Grid* grid, mt_rnd* rnd) {
    assert(grid);
    assert(rnd);
    grid_clear(grid);

    Allocator a;
    malloc_allocator(&a);

    GridPoint *activeList = alloc_mem(sizeof (GridPoint) * (grid->num_squares + 1), &a);
    GridPoint *listEnd = activeList;

    GridPoint start = {
            .x = mt_rnd_uint(rnd) % grid->cols,
            .y = mt_rnd_uint(rnd) % grid->rows,
    };

    grid_at(grid, start)->flags |= SQUARE_VISITED;
    *listEnd = start;
    ++listEnd;

    GridPoint neighbors[4];
    GridPoint unlinkedNeighbors[4];
    size_t numNeighbors;
    size_t numUnlinkedNeighbors;

    while (listEnd != activeList) {
        size_t cellIndex = mt_rnd_uint(rnd) % (listEnd - activeList);
        GridPoint cell = activeList[cellIndex];

        numNeighbors = grid_get_neighbors(grid, &cell, neighbors, 4);
        numUnlinkedNeighbors = 0;

        for (size_t n = 0; n < numNeighbors; ++n) {
            if ((grid_at(grid, neighbors[n])->flags & 0xf) == 0xf) {
                unlinkedNeighbors[numUnlinkedNeighbors++] = neighbors[n];
            }
        }

        if (numUnlinkedNeighbors > 0) {
            GridPoint neighbor = unlinkedNeighbors[mt_rnd_uint(rnd) % numUnlinkedNeighbors];
            grid_carve_to(grid, cell, neighbor);
            *listEnd = neighbor;
            ++listEnd;
        }
        else {
            activeList[cellIndex] = *(listEnd - 1);
            --listEnd;
        }
    }

    free_mem(activeList, &a);
    release_allocator(&a);
}

void maze_weighted_prims(Grid* grid, mt_rnd* rnd) {
    assert(grid);
    assert(rnd);
    grid_clear(grid);

    Allocator a;
    malloc_allocator(&a);

    for (size_t i = 0; i < grid->num_squares; ++i) {
        grid->squares[i].distance = mt_rnd_uint(rnd) % 100;
    }

    GridPoint *activeList = alloc_mem(sizeof (GridPoint) * (grid->num_squares + 1), &a);
    GridPoint *listEnd = activeList;

    GridPoint start = {
            .x = mt_rnd_uint(rnd) % grid->cols,
            .y = mt_rnd_uint(rnd) % grid->rows,
    };

    grid_at(grid, start)->flags |= SQUARE_VISITED;
    *listEnd = start;
    ++listEnd;

    GridPoint neighbors[4];
    GridPoint unlinkedNeighbors[4];
    size_t numNeighbors;
    size_t numUnlinkedNeighbors;

    while (listEnd != activeList) {
        size_t cellIndex = 0;
        int minVal = 10000;
        for (size_t i = 0; i < listEnd - activeList; ++i) {
            GridPoint p = activeList[i];
            if (grid_at(grid, p)->distance < minVal) {
                minVal = grid_at(grid, p)->distance;
                cellIndex = i;
            }
        }
        assert(minVal < 100);

        GridPoint cell = activeList[cellIndex];

        numNeighbors = grid_get_neighbors(grid, &cell, neighbors, 4);
        numUnlinkedNeighbors = 0;

        for (size_t n = 0; n < numNeighbors; ++n) {
            if ((grid_at(grid, neighbors[n])->flags & 0xf) == 0xf) {
                unlinkedNeighbors[numUnlinkedNeighbors++] = neighbors[n];
            }
        }

        if (numUnlinkedNeighbors > 0) {
            GridPoint neighbor;
            minVal = 10000;
            for (size_t n = 0; n < numUnlinkedNeighbors; ++n) {
                if (grid_at(grid, unlinkedNeighbors[n])->distance < minVal) {
                    minVal = grid_at(grid, unlinkedNeighbors[n])->distance;
                    neighbor = unlinkedNeighbors[n];
                }
            }
            assert(minVal < 100);
            grid_carve_to(grid, cell, neighbor);
            *listEnd = neighbor;
            ++listEnd;
        }
        else {
            activeList[cellIndex] = *(listEnd - 1);
            --listEnd;
        }
    }

    free_mem(activeList, &a);
    release_allocator(&a);

    grid_clear_solve(grid);
}

static void ellers_fill_set(Grid* grid, int existingSet, int newSet, size_t maxRow) {
    assert(grid);
    assert(existingSet > -1);
    assert(newSet > -1);
    assert(maxRow <= grid->rows);
    for (size_t row = 0; row < grid->rows && row <= maxRow; ++row) {
        for (size_t x = 0; x < grid->cols; ++x) {
            if (grid_xy(grid, x, row)->distance == existingSet) {
                grid_xy(grid, x, row)->distance = newSet;
            }
        }
    }
}

void maze_ellers(Grid* grid, mt_rnd* rnd) {
    assert(grid);
    assert(rnd);
    grid_clear(grid);

    Allocator a;
    malloc_allocator(&a);

    int* setList = alloc_mem(sizeof (int) * grid->cols, &a);
    size_t numSets = 0;

    GridPoint* elemsInSet = alloc_mem(sizeof (GridPoint) * grid->cols, &a);
    size_t numElemsInSet = 0;

    int nextSetId = 0;

    for (size_t row = 0; row < grid->rows - 1; ++row) {
        // create sets for row
        for (size_t x = 0; x < grid->cols; ++x) {
            if (grid_xy(grid, x, row)->distance < 0) {
                grid_xy(grid, x, row)->distance = nextSetId++;
            }
        }

        // randomly link with west neighbor
        for (size_t x = 0; x < grid->cols - 1; ++x) {
            GridPoint cur = {.x = x, .y = row};
            GridPoint neighbor = {.x = x + 1, .y = row};
            if (grid_at(grid, cur)->distance != grid_at(grid, neighbor)->distance && mt_rnd_float(rnd) < 0.5f) {
                grid_carve_to(grid, cur, neighbor);
                ellers_fill_set(grid, grid_at(grid, neighbor)->distance, grid_at(grid, cur)->distance, row);
            }
        }

        // Get a list of active sets
        numSets = 0;
        for (size_t x = 0; x < grid->cols; ++x) {
            int set = grid_xy(grid, x, row)->distance;
            bool inSetList = false;
            for (size_t s = 0; s < numSets; ++s) {
                inSetList |= setList[s] == set;
            }

            if (!inSetList) {
                assert(numSets < grid->cols);
                setList[numSets++] = set;
            }
        }

        // For each set, choose a path down
        for (size_t s = 0; s < numSets; ++s) {

            // Get the elements in the set
            numElemsInSet = 0;
            int set = setList[s];
            for (size_t x = 0; x < grid->cols; ++x) {
                GridPoint cur = {.x = x, .y = row};
                if (grid_at(grid, cur)->distance == set) {
                    elemsInSet[numElemsInSet++] = cur;
                }
            }

            // Choose a random set element to move down
            size_t numMovesDown = mt_rnd_uint(rnd) % (numElemsInSet) + 1;
            for (size_t m = 0; m < numMovesDown; ++m) {
                assert(numElemsInSet > 0);
                GridPoint connectionSouth = elemsInSet[mt_rnd_uint(rnd) % numElemsInSet];
                GridPoint south = {.x = connectionSouth.x, .y = row + 1};
                grid_carve_to(grid, connectionSouth, south);
                grid_at(grid, south)->distance = set;
            }
        }
    }

    // Last row, connect neighbors

    // create sets for row
    for (size_t x = 0; x < grid->cols; ++x) {
        if (grid_xy(grid, x, grid->rows - 1)->distance < 0) {
            grid_xy(grid, x, grid->rows - 1)->distance = nextSetId++;
        }
    }
    for (size_t x = 0; x < grid->cols - 1; ++x) {
        GridPoint cur = {.x = x, .y = grid->rows - 1};
        GridPoint neighbor = {.x = x + 1, .y = grid->rows - 1};
        if (grid_at(grid, cur)->distance != grid_at(grid, neighbor)->distance) {
            grid_carve_to(grid, cur, neighbor);
            ellers_fill_set(grid, grid_at(grid, neighbor)->distance, grid_at(grid, cur)->distance, grid->rows);
        }
    }

    free_mem(setList, &a);
    free_mem(elemsInSet, &a);

    release_allocator(&a);
    grid_clear_solve(grid);
}

static void rd_divide(Grid* grid, mt_rnd* rnd, size_t row, size_t column, size_t width, size_t height);

void maze_recursive_division(Grid* grid, mt_rnd* rnd) {
    assert(grid);
    assert(rnd);
    Square blank = {.flags = 0x0, .distance = -1 };
    grid_fill(grid, blank);
    rd_divide(grid, rnd, 0, 0, grid->rows, grid->cols);
    grid_ensure_border_walls(grid);
}

static void rd_divide_horiz(Grid* grid, mt_rnd* rnd, size_t row, size_t column, size_t width, size_t height);
static void rd_divide_vert(Grid* grid, mt_rnd* rnd, size_t row, size_t column, size_t width, size_t height);

static void rd_divide(Grid* grid, mt_rnd* rnd, size_t row, size_t column, size_t width, size_t height) {
    assert(grid);
    assert(rnd);
    if (width <= 1 || height <= 1 || (width <= 6 && height <= 6 && mt_rnd_uint(rnd) % 4 == 0)) {
        return;
    }

    if (height > width) {
        rd_divide_horiz(grid, rnd, row, column, width, height);
    }
    else {
        rd_divide_vert(grid, rnd, row, column, width, height);
    }
}

void rd_divide_horiz(Grid* grid, mt_rnd* rnd, size_t row, size_t column, size_t width, size_t height) {
    assert(grid);
    assert(rnd);
    assert(width > 1);
    assert(height > 1);
    size_t divSouthOffset = mt_rnd_uint(rnd) % (height - 1);
    size_t passageOffset = mt_rnd_uint(rnd) % (width - 1);

    size_t y = divSouthOffset + row;

    for (size_t offset = 0; offset < width; ++offset) {
        if (offset == passageOffset) {
            continue;
        }

        size_t x = offset + column;
        grid_separate(grid, x, y, WALL_SOUTH);
    }

    rd_divide(grid, rnd, row, column, width, divSouthOffset + 1);
    rd_divide(grid, rnd, divSouthOffset + 1 + row, column, width, height - divSouthOffset - 1);
}

void rd_divide_vert(Grid* grid, mt_rnd* rnd, size_t row, size_t column, size_t width, size_t height) {
    assert(grid);
    assert(rnd);
    assert(width > 1);
    assert(height > 1);
    size_t divEastOffset = mt_rnd_uint(rnd) % (width - 1);
    size_t passageOffset = mt_rnd_uint(rnd) % (height - 1);
    size_t x = divEastOffset + column;

    for (size_t offset = 0; offset < height; ++offset) {
        size_t y = offset + row;
        if (offset == passageOffset) {
            continue;
        }

        grid_separate(grid, x, y, WALL_EAST);
    }

    rd_divide(grid, rnd, row, column, divEastOffset + 1, height);
    rd_divide(grid, rnd, row, divEastOffset + 1 + column, width - divEastOffset - 1, height);
}
