#ifndef SDL_DEMO_SOLVER_H
#define SDL_DEMO_SOLVER_H

#include "grid.h"

#ifdef __cplusplus
extern "C" {
#endif

int solver_solve(Grid *grid, GridPoint start, GridPoint end, GridPoint* queue, size_t queueSize);

#ifdef __cplusplus
};
#endif

#endif //SDL_DEMO_SOLVER_H
