#include "mem.h"
#include <malloc.h>
#include <assert.h>

static void* malloc_impl(Allocator* self, size_t numBytes) {
#ifdef NDEBUG
    return malloc(numBytes);
#else
    void* ptr = malloc(numBytes + sizeof(uint64_t));
    *((uint64_t*)ptr) = numBytes;
    self->allocated += numBytes;
    return (void*)(((char*)ptr) + sizeof(uint64_t));
#endif
}

static void free_impl(Allocator* self, void* ptr) {
#ifdef NDEBUG
    return free(ptr);
#else
    ptr = (void*)(((char*)ptr) - sizeof(uint64_t));
    assert(self->allocated >= *((uint64_t*)ptr));
    self->allocated -= *((uint64_t*)ptr);
    return free(ptr);
#endif
}

void malloc_allocator(Allocator* outAlloc) {
    assert(outAlloc);
    Allocator res = {
            .alloc = malloc_impl,
            .free = free_impl,
            .allocated = 0
    };
    *outAlloc = res;
}

static void* calloc_impl(Allocator * self, size_t numBytes) {
#ifdef NDEBUG
    return calloc(numBytes, 1);
#else
    void* ptr = calloc(numBytes + sizeof(uint64_t), 1);
    *((uint64_t*)ptr) = numBytes;
    self->allocated += numBytes;
    return (void*)(((char*)ptr) + sizeof(uint64_t));
#endif
}

void mem_calloc_allocator(Allocator* outAlloc) {
    assert(outAlloc);
    Allocator res = {
            .alloc = calloc_impl,
            .free = free_impl,
            .allocated = 0
    };
    *outAlloc = res;
}

void release_allocator(Allocator* allocator) {
    if (allocator->destroy) {
        allocator->destroy(allocator);
    }
    assert(allocator->allocated == 0);
}

void* alloc_mem(size_t size, Allocator* allocator) {
    return allocator->alloc(allocator, size);
}

void free_mem(void* mem, Allocator* allocator) {
    allocator->free(allocator, mem);
}
