#ifndef SDL_DEMO_GRID_H
#define SDL_DEMO_GRID_H

#include <stdbool.h>
#include <stddef.h>
#include "mem.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum SquareWalls {
    WALL_NORTH = 0x1,
    WALL_SOUTH = 0x2,
    WALL_EAST = 0x4,
    WALL_WEST = 0x8,
} SquareWalls;

enum SquareProperties {
    SQUARE_START = 0x10,
    SQUARE_END = 0x20,
    SQUARE_ON_PATH = 0x40,
};

typedef struct Square {
    unsigned flags;
    int distance;
} Square;

typedef struct Grid {
    size_t rows;
    size_t cols;
    size_t num_squares;
    Square *squares;
} Grid;

typedef struct GridPoint {
    size_t x;
    size_t y;
} GridPoint;


void grid_init_from(Grid *outGrid, Square * squares, size_t numSquares, size_t rows, size_t cols);

void grid_init(Grid *outGrid, size_t maxX, size_t maxY, Allocator* alloc);

void grid_free(Grid *grid, Allocator* alloc);

Square *grid_xy(const Grid *grid, size_t x, size_t y);

Square *grid_at(const Grid *grid, GridPoint p);

Square *grid_index(const Grid* grid, size_t index);

void grid_fill(Grid *grid, Square with);

void grid_clear(Grid *grid);

void grid_clear_solve(Grid *grid);

bool grid_can_move_to(const Grid *grid, GridPoint a, GridPoint b);

void grid_carve(Grid *grid, size_t x, size_t y, SquareWalls wall);

void grid_separate(Grid *grid, size_t x, size_t y, SquareWalls wall);

void grid_ensure_border_walls(Grid *grid);

size_t grid_get_neighbors(const Grid *grid, const GridPoint *point, GridPoint *outPoints, size_t numOutPoints);

bool grid_carve_to(Grid *grid, GridPoint a, GridPoint b);

size_t grid_count_deadends(const Grid* grid);

#ifdef __cplusplus
};
#endif

#endif //SDL_DEMO_GRID_H
