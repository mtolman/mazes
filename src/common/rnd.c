#include "assert.h"
#include "rnd.h"

static void mt_rnd_period_cert(mt_rnd *rnd);
static void mt_rnd_next_state(mt_rnd *rnd);
static uint32_t mt_rnd_temper(const mt_rnd *rnd);
static uint32_t mt_rnd_temper_conv(const mt_rnd *rnd);

void mt_rnd_init(mt_rnd *rnd, uint32_t seed) {
  mt_rnd_init_mat(rnd, seed, 0x8f7011ee, 0xfc78ff1f, 0x3793fdff);
}

void mt_rnd_init_mat(mt_rnd *rnd, uint32_t seed, uint32_t mat1, uint32_t mat2,
                     uint32_t tmat) {
  assert(rnd);
  rnd->state[0] = seed;
  rnd->state[1] = mat1;
  rnd->state[2] = mat2;
  rnd->state[3] = tmat;

  for (uint32_t i = 1; i < 8; ++i) {
    const uint32_t xorL = rnd->state[(i - 1) & 3];
    const uint32_t xorR = rnd->state[(i - 1) & 3] >> 30;
    const uint32_t mult = 1812433253u * (xorL ^ xorR);
    const uint32_t sum = i + mult;
    rnd->state[i & 3] ^= sum;
  }
  mt_rnd_period_cert(rnd);
  for (uint32_t i = 0; i < 8; ++i) {
    mt_rnd_next_state(rnd);
  }
}

uint32_t mt_rnd_uint(mt_rnd *rnd) {
  assert(rnd);
  mt_rnd_next_state(rnd);
  return mt_rnd_temper(rnd);
}

int mt_rnd_int(mt_rnd *rnd) {
  union {
    uint32_t u;
    int i;
  } v;
  v.u = mt_rnd_uint(rnd);
  return v.i;
}

float mt_rnd_float(mt_rnd *rnd) {
  assert(rnd);
  const uint32_t v = mt_rnd_uint(rnd);
  return (float)((double)v / (double)(0xffffffff));
}

static void mt_rnd_period_cert(mt_rnd *rnd) {
  if ((rnd->state[0] & 0x7fffffff) == 0 && rnd->state[1] == 0 &&
      rnd->state[2] == 0 && rnd->state[3] == 0) {
    rnd->state[0] = 'M';
    rnd->state[1] = 'A';
    rnd->state[2] = 'T';
    rnd->state[3] = 'T';
  }
}

static void mt_rnd_next_state(mt_rnd *rnd) {
  uint32_t y = rnd->state[3];
  uint32_t x = (rnd->state[0] & 0x7fffffff) ^ rnd->state[1] ^ rnd->state[2];
  x ^= x << 1;
  y ^= (y >> 1) ^ x;
  rnd->state[0] = rnd->state[1];
  rnd->state[1] = rnd->state[2];
  rnd->state[2] = x ^ (y << 10);
  rnd->state[3] = y;

  const uint32_t a = -(y & 1) & rnd->mat1;
  const uint32_t b = -(y & 1) & rnd->mat2;
  rnd->state[1] ^= a;
  rnd->state[2] ^= b;
}

static uint32_t mt_rnd_temper(const mt_rnd *rnd) {
  uint32_t t0 = rnd->state[3];
  uint32_t t1 = rnd->state[0] + (rnd->state[2] >> 8);
  t0 ^= t1;
  if (t1 & 1) {
    t0 ^= rnd->tmat;
  }
  return t0;
}
