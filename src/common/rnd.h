#ifndef MT_RND
#define MT_RND

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

// Based on the TinyMT algorithm

typedef struct mt_rnd {
  uint32_t state[4];
  uint32_t mat1;
  uint32_t mat2;
  uint32_t tmat;
} mt_rnd;

void mt_rnd_init(mt_rnd *rnd, uint32_t seed);
void mt_rnd_init_mat(mt_rnd *rnd, uint32_t seed, uint32_t mat1, uint32_t mat2,
                     uint32_t tmat);
uint32_t mt_rnd_uint(mt_rnd *rnd);
int mt_rnd_int(mt_rnd *rnd);
float mt_rnd_float(mt_rnd *rnd);

#ifdef __cplusplus
};
#endif

#endif
