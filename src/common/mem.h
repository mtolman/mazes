#ifndef SDL_DEMO_MEM_H
#define SDL_DEMO_MEM_H

#include <stddef.h>
#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Allocator {
    void* (*alloc)(struct Allocator* self, size_t size);
    void (*free)(struct Allocator* self, void* target);
    void (*destroy)(struct Allocator* self);
    size_t allocated;
} Allocator;

void malloc_allocator(Allocator* outAlloc);
void calloc_allocator(Allocator* outAlloc);

void release_allocator(Allocator*);

void* alloc_mem(size_t size, Allocator* allocator);
void free_mem(void* mem, Allocator* allocator);

#ifdef __cplusplus
};
#endif

#endif //SDL_DEMO_MEM_H
